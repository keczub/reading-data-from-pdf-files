## Main idea
Most of the aeronautical information delivered by the national agencies are published in the .pdf file format. Some of them are organized in structured way that is unchanged across AIRAC cycles. It enables a reader to read information much easier. Structured form also makes possibility to automatically collect this information to computer readable form and import it into system. Automatically extracted data can help aeronautical information analyst reduce time of changes interpretation. 

## Repository diagram
![Alt text](images/Project diagram.png?raw=true "Title")
